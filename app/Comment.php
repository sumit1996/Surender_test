<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'hotal_id', 'comment',
    ];

    /**
    *hotel relationship
    */
    public function comments(){
    	return $this->belongsTo(Hotel::class,'hotal_id','id');
    }

}
