<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Hotel,Comment};
class CommentController extends Controller
{   
	/**
	*add hotel Comment
	*/
    public function addComment(Request $res){
    $hotel = Hotel::findOrFail($res->id);
    $hotel->comments()->create(['comment'=>$res->comment]);
    return redirect()->back()->with('message','comment added successfully!');
    }
}
