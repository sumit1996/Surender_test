<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hotel;
class HotelController extends Controller
{
    /**
    *get comments
    */

    public function getComments(Request $request){
      
       return view('comments',['id'=>$request->id]); 
    }

}
