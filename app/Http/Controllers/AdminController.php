<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Hotel;
class AdminController extends Controller
{   
   

  
    /**
    *hotel form
    */
    public function hotelform(){
    	if (Auth::user()->id != 1) {
        	dd('unauthorised access!');
        }
    	return view('create_hotel');
    }

    /**
    *store hotels
    */

    public function addHotel(Request $request){
        Hotel::create(['hotal_name'=>$request->hotal_name]);
        return redirect()->back()->with('message','Hotel Added Successfully!');
    }

}
