<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class Hotel extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'hotal_name',
    ];

    /**
    *comment relationship
    */
    public function comments(){
    	return $this->hasMany(Comment::class,'hotal_id','id');
    }

}
