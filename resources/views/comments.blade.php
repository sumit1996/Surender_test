@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                
                @if(Auth::check())
                @if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
<form class="form-horizontal" action="{{route('add-comment')}}" method="POST">
    <input type="hidden" name="id" value="{{$id}}">
  <div class="form-group">
    <label class="control-label col-sm-2" for="name">comment:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="comment" id="name" placeholder="...">
    </div>
  </div>
@csrf
 
  <div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">post</button>
    </div>
  </div>
</form>

                @endif

                <ul><?php
                $hotel = App\Hotel::findOrFail($id);
                
                ?>
                    <h2>Hotels name:{{$hotel->hotal_name}}</h2>
                  <h5>  comments:</h5>
                @foreach($hotel->comments as $comment)
                <li>{{$comment->comment}}</li>
                @endforeach
            </ul>

            </div>
        </div>
    </div>
</div>
@endsection
