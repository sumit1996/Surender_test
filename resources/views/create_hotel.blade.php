@extends('layouts.app')

@section('content')
@if (Session::has('message'))
	<div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
<form class="form-horizontal" action="{{route('add-hotel')}}" method="POST">
  <div class="form-group">
    <label class="control-label col-sm-2" for="name">Hotal Name:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="hotal_name" id="name" placeholder="Hotel Name">
    </div>
  </div>
@csrf
 
  <div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Create</button>
    </div>
  </div>
</form>

@endsection