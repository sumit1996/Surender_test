@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                   
                </div>
                @if(Auth::user()->id === 1)
                <a href="{{route('create-hotel')}}">Create Hotel</a>
                @else
                <ul>
                    <h2>Hotels list:</h2>
                @foreach(App\Hotel::all() as $hotel)
                <li><a href="{{route('hotel-comments')}}?id={{$hotel->id}}">{{$hotel->hotal_name}}</a></li>
                @endforeach
            </ul>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
