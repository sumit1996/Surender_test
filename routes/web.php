<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::group(array('before' => 'auth'), function()
{
Route::post('add-comment','CommentController@addComment')->name('add-comment');	
Route::get('create-hotel','AdminController@hotelform')->name('create-hotel');
Route::post('add-hotel','AdminController@addHotel')->name('add-hotel');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('hotel-comments','HotelController@getComments')->name('hotel-comments');
});